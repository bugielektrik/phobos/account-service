CREATE TABLE IF NOT EXISTS accounts (
    id          SERIAL PRIMARY KEY,
    created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    account_id  VARCHAR NOT NULL UNIQUE,
    type        INTEGER NOT NULL,
    phone       VARCHAR NULL,
    email       VARCHAR NULL,
    parent_id   VARCHAR NULL,
    name        VARCHAR NULL,
    latitude    NUMERIC NULL,
    longitude   NUMERIC NULL,
    is_active   BOOLEAN NOT NULL DEFAULT TRUE,
    is_manager  BOOLEAN NOT NULL DEFAULT FALSE
);