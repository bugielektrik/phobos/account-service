package api

import (
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/account-service/internal/config"
)

const idleTimeout = 5 * time.Second

func (h *Handler) InitRest(cfg *config.Config) *fiber.App {
	// Init rest api
	router := fiber.New(
		fiber.Config{
			IdleTimeout: idleTimeout,
		})

	//router.Use(
	//	cors.New(),
	//	helmet.New(),
	//	csrf.New(cfg.Csrf),
	//	limiter.New(cfg.Limiter),
	//	logger.New(),
	//)

	// Init router
	router.Get("/ping", func(c *fiber.Ctx) error {
		c.Status(http.StatusOK)
		return c.SendString("pong")
	})

	h.initRouter(router)

	return router
}

func (h *Handler) initRouter(router *fiber.App) {
	api := router.Group("/api/v1")
	{
		otp := api.Group("/otps")
		{
			otp.Get("", h.Account.getOTP)
			otp.Post("", h.Account.checkOTP)
		}

		store := api.Group("/stores")
		{
			store.Get("", h.Account.getNearestStores)
			store.Get("/:id", h.Account.getStoreById)
		}

		account := api.Group("/accounts")
		{
			account.Get("", h.Account.getAccountById)
			account.Put("", h.Account.updateAccount)

			accountStore := account.Group("/stores")
			{
				accountStore.Post("", h.Account.createStore)
				accountStore.Put("/:id", h.Account.updateStore)
				accountStore.Delete("/:id", h.Account.deleteStore)
			}
		}
	}
}
