package api

type Dependencies struct {
	AccountService AccountService
}

type Handler struct {
	Account *Account
}

func New(d Dependencies) *Handler {
	return &Handler{
		Account: NewAccountHandler(d.AccountService),
	}
}
