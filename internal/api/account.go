package api

import (
	"context"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"

	"gitlab.com/bugielektrik/phobos/account-service/internal/model"
	"gitlab.com/bugielektrik/phobos/account-service/pkg/client"
)

type AccountService interface {
	GetOTP(phone, debug string) (*client.OTP, error)
	CheckOTP(otpDest *client.OTP) (*client.Oauth, error)
	GetAccountByID(id string) (*model.Account, error)
	UpdateAccount(ctx context.Context, id string, accountDest *model.Account) (*model.Account, error)
	GetNearestStores(latitude, longitude string) ([]*model.Account, error)
	GetStoreByID(id string) (*model.Account, error)
	CreateStore(merchantID string, storeDest *model.Account) (*model.Account, error)
	UpdateStore(ctx context.Context, id, merchantID string, storeDest *model.Account) (*model.Account, error)
	DeleteStore(ctx context.Context, id, merchantID string) (*model.Account, error)
}

type Account struct {
	service AccountService
}

func NewAccountHandler(s AccountService) *Account {
	return &Account{service: s}
}

func (h *Account) getOTP(c *fiber.Ctx) error {
	// Bind params
	phone := c.Query("phone")
	debug := c.Query("debug")

	// Read data from database
	otpSrc, err := h.service.GetOTP(phone, debug)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(otpSrc)
}

func (h *Account) checkOTP(c *fiber.Ctx) error {
	// Bind params
	otpDest := new(client.OTP)
	if err := c.BodyParser(otpDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Validate params
	if err := validator.New().Struct(otpDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	oauthSrc, err := h.service.CheckOTP(otpDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(oauthSrc)
}

func (h *Account) getAccountById(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	// Read data from database
	accountSrc, err := h.service.GetAccountByID(accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if accountSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.JSON(accountSrc)
}

func (h *Account) updateAccount(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	// Bind params
	accountDest := new(model.Account)
	err := c.BodyParser(accountDest)
	if err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	ctx := context.WithValue(c.Context(), "id", accountID)
	accountSrc, err := h.service.UpdateAccount(ctx, accountID, accountDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if accountSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (h *Account) getNearestStores(c *fiber.Ctx) error {
	// Bind params
	latitude := c.Query("latitude")
	longitude := c.Query("longitude")

	// Read data from database
	storesSrc, err := h.service.GetNearestStores(latitude, longitude)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(storesSrc)
}

func (h *Account) getStoreById(c *fiber.Ctx) error {
	// Bind params
	storeID := c.Params("id")

	// Read data from database
	storeSrc, err := h.service.GetStoreByID(storeID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if storeSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.JSON(storeSrc)
}

func (h *Account) createStore(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")

	storeDest := new(model.Account)
	err := c.BodyParser(storeDest)
	if err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Validate params
	if err := validator.New().Struct(storeDest); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	orderSrc, err := h.service.CreateStore(accountID, storeDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	c.Status(fiber.StatusCreated)
	return c.JSON(orderSrc)
}

func (h *Account) updateStore(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")
	storeID := c.Params("id")

	storeDest := new(model.Account)
	err := c.BodyParser(storeDest)
	if err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	// Write data to DB
	ctx := context.WithValue(c.Context(), "id", storeID)
	storeSrc, err := h.service.UpdateStore(ctx, storeID, accountID, storeDest)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if storeSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (h *Account) deleteStore(c *fiber.Ctx) error {
	// Bind params
	accountID := c.Get("X-Account-ID")
	storeID := c.Params("id")

	// Delete data from DB
	ctx := context.WithValue(c.Context(), "id", storeID)
	storeSrc, err := h.service.DeleteStore(ctx, storeID, accountID)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{"error": err.Error()})
	}

	if storeSrc == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.SendStatus(fiber.StatusOK)
}
