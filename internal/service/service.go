package service

import "gitlab.com/bugielektrik/phobos/account-service/pkg/client"

type Dependencies struct {
	OTPClient    *client.OTPClient
	OauthClient  *client.OauthClient
	AccountStore AccountStore
}

type Service struct {
	Account *Account
}

func New(d Dependencies) *Service {
	return &Service{
		Account: NewAccountService(d.OTPClient, d.OauthClient, d.AccountStore),
	}
}
