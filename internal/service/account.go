package service

import (
	"context"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"

	"gitlab.com/bugielektrik/phobos/account-service/internal/model"
	"gitlab.com/bugielektrik/phobos/account-service/pkg/client"
)

type AccountStore interface {
	Create(data *model.Account) error
	GetByID(id string) (*model.Account, error)
	GetByPhone(phone string) (*model.Account, error)
	GetStoreByID(id string) (*model.Account, error)
	GetMerchantStoreByID(id, merchantID string) (*model.Account, error)
	GetNearestStores(latitude, longitude decimal.Decimal) ([]*model.Account, error)
	Lock(ctx context.Context, id string) (func(), *sqlx.Tx, error)
	Update(ctx context.Context, tx *sqlx.Tx, id string, data *model.Account) error
}

type Account struct {
	otp   *client.OTPClient
	oauth *client.OauthClient
	store AccountStore
}

func NewAccountService(otp *client.OTPClient, oauth *client.OauthClient, s AccountStore) *Account {
	return &Account{
		otp:   otp,
		oauth: oauth,
		store: s,
	}
}

func (s *Account) GetOTP(phone, debug string) (*client.OTP, error) {
	otpSrc, err := s.otp.GetOTP(phone, debug)
	return otpSrc, err
}

func (s *Account) CheckOTP(otpDest *client.OTP) (*client.Oauth, error) {
	err := s.otp.CheckOTP(otpDest)
	if err != nil {
		return nil, err
	}

	accountSrc, err := s.store.GetByPhone(otpDest.Phone)
	if err != nil {
		return nil, err
	}

	if accountSrc == nil {
		accountIns := &model.Account{
			Type:      model.CUSTOMER,
			IsActive:  &[]bool{true}[0],
			IsManager: &[]bool{false}[0],
			Phone:     &[]string{otpDest.Phone}[0],
		}

		// Generate data ID
		for {
			id := uuid.New().String()

			accountSrc, err = s.store.GetByID(id)
			if err != nil {
				return nil, err
			}

			if accountSrc == nil {
				accountIns.ID = id
				break
			}
		}

		err = s.store.Create(accountIns)
		if err != nil {
			return nil, err
		}

		accountSrc = accountIns
	}

	oauthIns := &client.Oauth{
		AccountID:   accountSrc.ID,
		AccountType: accountSrc.Type,
	}

	oauthSrc, err := s.oauth.AuthorizeAccount(oauthIns)
	return oauthSrc, err
}

func (s *Account) GetAccountByID(id string) (*model.Account, error) {
	accountSrc, err := s.store.GetByID(id)
	return accountSrc, err
}

func (s *Account) UpdateAccount(ctx context.Context, id string, accountDest *model.Account) (*model.Account, error) {
	accountSrc, err := s.store.GetByID(id)
	if err != nil {
		return nil, err
	}

	if accountSrc == nil {
		return nil, nil
	}

	unlockStore, reqTx, err := s.store.Lock(ctx, id)
	if err != nil {
		return nil, err
	}
	defer unlockStore()

	err = s.store.Update(ctx, reqTx, id, accountDest)
	if err != nil {
		return nil, err
	}

	return accountSrc, nil
}

func (s *Account) GetNearestStores(latitude, longitude string) ([]*model.Account, error) {
	lat, err := decimal.NewFromString(latitude)
	if err != nil {
		return nil, err
	}

	long, err := decimal.NewFromString(longitude)
	if err != nil {
		return nil, err
	}

	storesSrc, err := s.store.GetNearestStores(lat, long)
	return storesSrc, err
}

func (s *Account) GetStoreByID(id string) (*model.Account, error) {
	storeSrc, err := s.store.GetStoreByID(id)
	return storeSrc, err
}

func (s *Account) CreateStore(merchantID string, storeDest *model.Account) (*model.Account, error) {
	// Generate data ID
	for {
		id := uuid.New().String()

		storeSrc, err := s.store.GetStoreByID(id)
		if err != nil {
			return nil, err
		}

		if storeSrc == nil {
			storeDest.ID = id
			break
		}
	}

	storeDest.Type = model.STORE
	storeDest.IsActive = &[]bool{true}[0]
	storeDest.IsManager = &[]bool{false}[0]
	storeDest.ParentID = &[]string{merchantID}[0]

	err := s.store.Create(storeDest)
	return storeDest, err
}

func (s *Account) UpdateStore(ctx context.Context, id, merchantID string, storeDest *model.Account) (*model.Account, error) {
	storeSrc, err := s.store.GetMerchantStoreByID(id, merchantID)
	if err != nil {
		return nil, err
	}

	if storeSrc == nil {
		return nil, nil
	}

	unlockStore, reqTx, err := s.store.Lock(ctx, id)
	if err != nil {
		return nil, err
	}
	defer unlockStore()

	err = s.store.Update(ctx, reqTx, id, storeDest)
	if err != nil {
		return nil, err
	}

	return storeSrc, nil
}

func (s *Account) DeleteStore(ctx context.Context, id, merchantID string) (*model.Account, error) {
	storeSrc, err := s.store.GetMerchantStoreByID(id, merchantID)
	if err != nil {
		return nil, err
	}

	if storeSrc == nil {
		return nil, nil
	}

	unlockStore, reqTx, err := s.store.Lock(ctx, id)
	if err != nil {
		return nil, err
	}
	defer unlockStore()

	storeIns := &model.Account{
		IsActive: &[]bool{false}[0],
	}

	err = s.store.Update(ctx, reqTx, id, storeIns)
	if err != nil {
		return nil, err
	}

	return storeSrc, nil
}
