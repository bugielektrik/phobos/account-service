package model

import (
	"time"

	"github.com/shopspring/decimal"
)

const (
	CUSTOMER = 1
	MERCHANT = 2
	STORE    = 3
	CASHIER  = 4
	COURIER  = 5
)

type Account struct {
	ID         string           `json:"id,omitempty" db:"account_id"`
	Type       int              `json:"type,omitempty" db:"type"`
	Phone      *string          `json:"phone,omitempty" db:"phone"`
	Email      *string          `json:"email,omitempty" db:"email"`
	ParentID   *string          `json:"parent_id,omitempty" db:"parent_id"`
	Name       *string          `json:"name,omitempty" db:"name" validate:"required"`
	Latitude   *decimal.Decimal `json:"latitude,omitempty" db:"latitude" validate:"required"`
	Longitude  *decimal.Decimal `json:"longitude,omitempty" db:"longitude" validate:"required"`
	IsActive   *bool            `json:"is_active,omitempty" db:"is_active"`
	IsManager  *bool            `json:"is_manager,omitempty" db:"is_manager"`
	InternalID string           `json:"-" db:"account_id"`
	CreatedAt  time.Time        `json:"-" db:"created_at"`
	UpdatedAt  time.Time        `json:"-" db:"updated_at"`
}
