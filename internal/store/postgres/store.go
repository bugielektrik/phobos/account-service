package postgres

import (
	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"
)

type Store struct {
	Account *Account
}

func New(db *sqlx.DB, logger hclog.Logger) *Store {
	return &Store{
		Account: NewAccountStore(db, logger),
	}
}
