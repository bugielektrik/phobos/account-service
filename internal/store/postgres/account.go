package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"
	"github.com/shopspring/decimal"

	"gitlab.com/bugielektrik/phobos/account-service/internal/model"
)

type Account struct {
	db     *sqlx.DB
	logger hclog.Logger
}

func NewAccountStore(db *sqlx.DB, logger hclog.Logger) *Account {
	return &Account{
		db:     db,
		logger: logger,
	}
}

func (s *Account) Create(data *model.Account) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := s.db.NamedExecContext(ctx, `
		INSERT INTO accounts (account_id, type, phone, email, parent_id, name, latitude, longitude, is_active, is_manager)
		VALUES(:account_id, :type, :phone, :email, :parent_id, :name, :latitude, :longitude, :is_active, :is_manager)`,
		data)
	return err
}

func (s *Account) GetByID(id string) (*model.Account, error) {
	query := `
		SELECT *
		FROM accounts 
		WHERE account_id=:account_id`
	args := map[string]interface{}{
		"account_id": id,
	}
	data, err := s.getRow("GetByID", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Account) GetByPhone(phone string) (*model.Account, error) {
	query := `
		SELECT account_id, type
		FROM accounts 
		WHERE phone=:phone`
	args := map[string]interface{}{
		"phone": phone,
	}
	data, err := s.getRow("GetByPhone", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Account) GetStoreByID(id string) (*model.Account, error) {
	query := `
		SELECT account_id, name, latitude, longitude, is_active
		FROM accounts 
		WHERE account_id=:account_id AND type=:type`
	args := map[string]interface{}{
		"account_id": id,
		"type":       model.STORE,
	}
	data, err := s.getRow("GetStoreByID", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Account) GetMerchantStoreByID(id, parentID string) (*model.Account, error) {
	query := `
		SELECT *
		FROM accounts 
		WHERE account_id=:account_id AND parent_id=:parent_id AND type=:type`
	args := map[string]interface{}{
		"account_id": id,
		"type":       model.STORE,
		"parent_id":  parentID,
	}
	data, err := s.getRow("GetMerchantStoreByID", query, args)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Account) GetNearestStores(latitude, longitude decimal.Decimal) ([]*model.Account, error) {
	query := `
		SELECT account_id, name, latitude, longitude
		FROM accounts
		WHERE type=3 AND is_active='true' AND acos(sin(` + latitude.String() + `) * sin(latitude) + cos(` + latitude.String() + `) * cos(latitude) * cos(longitude - (` + longitude.String() + `))) * 6371 <= 1000`
	data, err := s.getRows("GetNearestStores", query)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (s *Account) Update(ctx context.Context, tx *sqlx.Tx, id string, data *model.Account) error {
	setValues := make([]string, 0)

	if data.Name != nil {
		setValues = append(setValues, "name=:name")
	}

	if data.Latitude != nil {
		setValues = append(setValues, "latitude=:latitude")
	}

	if data.Longitude != nil {
		setValues = append(setValues, "longitude=:longitude")
	}

	if data.IsActive != nil {
		setValues = append(setValues, "is_active=:is_active")
	}

	if data.IsManager != nil {
		setValues = append(setValues, "is_manager=:is_manager")
	}

	setValues = append(setValues, "updated_at=CURRENT_TIMESTAMP")

	setQuery := strings.Join(setValues, ", ")

	query := fmt.Sprintf("UPDATE accounts SET %s WHERE account_id='%s'", setQuery, id)

	err := s.update(ctx, tx, "Update", query, data)
	return err
}

func (s *Account) Lock(ctx context.Context, id string) (func(), *sqlx.Tx, error) {
	logger := s.logger.With("operation", "Lock", "id", id)

	query := `SELECT * FROM accounts WHERE account_id=:account_id FOR UPDATE`
	args := map[string]interface{}{
		"account_id": id,
	}

	logger.Debug("begin transaction")
	tx, err := s.db.BeginTxx(ctx, nil)
	if err != nil {
		logger.Error("failed to begin transaction", "error", err)
		return nil, nil, err
	}

	rows, err := tx.NamedQuery(query, args)
	if err != nil {
		logger.Error("failed to execute statement", "error", err)
		return nil, nil, err
	}
	defer rows.Close()

	logger.Debug("locked")
	return func() {
		err := tx.Commit()
		if err != nil {
			logger.Error("error while unlocking", "error", err)
			return
		}

		logger.Debug("unlocked")
	}, tx, nil
}

func (s *Account) getRow(operation, query string, args interface{}) (*model.Account, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	row, err := s.db.NamedQueryContext(ctx, query, args)
	if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer row.Close()

	if !row.Next() {
		logger.Debug("not found")
		return nil, nil
	}

	data := new(model.Account)
	err = row.StructScan(&data)
	if err != nil {
		logger.Error("failed to scan into struct", "error", err)
		return nil, err
	}

	return data, nil
}

func (s *Account) getRows(operation, query string) ([]*model.Account, error) {
	logger := s.logger.With("operation", operation)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rows, err := s.db.QueryxContext(ctx, query)
	if err == sql.ErrNoRows {
		logger.Debug("not found")
		return nil, nil
	} else if err != nil {
		logger.Error("failed to get", "error", err)
		return nil, err
	}
	defer rows.Close()

	dataArr := make([]*model.Account, 0)
	for rows.Next() {
		data := new(model.Account)
		err = rows.StructScan(&data)
		if err != nil {
			logger.Error("failed to scan into struct", "error", err)
			return nil, err
		}
		dataArr = append(dataArr, data)
	}

	return dataArr, nil
}

func (s *Account) update(ctx context.Context, tx *sqlx.Tx, operation, query string, args interface{}) error {
	logger := s.logger.With("operation", operation)

	logger.Debug("start exec")
	finalize := func() error { return nil }
	if tx == nil {
		ttx, err := s.db.BeginTxx(ctx, nil)
		if err != nil {
			// failed to start transaction
			logger.Error("failed to start transaction", "error", err)
			return err
		}
		finalize = func() error {
			err = ttx.Commit()
			if err != nil {
				logger.Error("failed to commit", "error", err)
				return err
			}

			logger.Debug("committed update")
			return nil
		}
		tx = ttx
	}

	_, err := tx.NamedExec(query, args)
	if err != nil {
		logger.Error("failed to update", "error", err)
		return err
	}

	logger.Info("updated")
	return finalize()
}
