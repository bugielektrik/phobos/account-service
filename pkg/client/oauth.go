package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

type Oauth struct {
	AccountID    string `json:"account_id" db:"account_id"`
	AccountType  int    `json:"account_type,omitempty" db:"account_type"`
	AccessToken  string `json:"access_token,omitempty" db:"access_token"`
	ExpiresIn    string `json:"expires_in,omitempty" db:"expires_in"`
	TokenType    string `json:"token_type,omitempty" db:"token_type"`
	RefreshToken string `json:"refresh_token,omitempty" db:"refresh_token"`
}

type OauthClient struct {
	endpoint string
	client   *http.Client
}

func NewOauthClient(endpoint string) *OauthClient {
	client := http.DefaultClient
	client.Timeout = 30 * time.Second

	return &OauthClient{
		endpoint: endpoint,
		client:   client,
	}
}

func (s *OauthClient) AuthorizeAccount(oauthDest *Oauth) (*Oauth, error) {
	// marshal body
	reqBody, err := json.Marshal(oauthDest)
	if err != nil {
		return nil, err
	}

	// setup request
	url := s.endpoint + "/api/v1/oauths/authorize"
	resBody, status, err := s.oauthHandler("POST", url, reqBody)
	if err != nil {
		return nil, err
	}

	switch status {
	case 200:
		// unmarshal response data
		oauthSrc := new(Oauth)
		err = json.Unmarshal(resBody, &oauthSrc)
		if err != nil {
			return nil, err
		}

		return oauthSrc, nil
	case 400:
		return nil, errors.New("bad request")
	default:
		return nil, errors.New("external server error")
	}
}

func (s *OauthClient) CheckToken(accessToken string) (*Oauth, error) {
	// setup request
	url := s.endpoint + "/api/v1/oauths/token?access_token=" + accessToken
	resBody, status, err := s.oauthHandler("GET", url, nil)
	if err != nil {
		return nil, err
	}

	switch status {
	case 200:
		// unmarshal response data
		oauthSrc := new(Oauth)
		err = json.Unmarshal(resBody, &oauthSrc)
		if err != nil {
			return nil, err
		}

		return oauthSrc, nil
	case 404:
		return nil, errors.New("not found")
	default:
		return nil, errors.New("external server error")
	}
}

func (s *OauthClient) oauthHandler(method string, url string, body []byte) ([]byte, int, error) {
	// setup request
	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, 0, err
	}

	// setup request header
	req.Header.Add("Content-Type", "application/json")

	// send request
	res, err := s.client.Do(req)
	if err != nil {
		return nil, 0, err
	}

	// read response body
	defer res.Body.Close()
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, 0, err
	}

	return resBody, res.StatusCode, nil
}
