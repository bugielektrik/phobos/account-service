package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

type OTP struct {
	Key   string `json:"key" db:"key" validate:"required"`
	Phone string `json:"phone" db:"phone" validate:"required"`
	OTP   string `json:"otp,omitempty" validate:"required"`
}

type OTPClient struct {
	endpoint string
	client   *http.Client
}

func NewOTPClient(endpoint string) *OTPClient {
	client := http.DefaultClient
	client.Timeout = 30 * time.Second

	return &OTPClient{
		endpoint: endpoint,
		client:   client,
	}
}

func (s *OTPClient) GetOTP(phone, debug string) (*OTP, error) {
	// setup request
	url := s.endpoint + "/api/v1/otps?phone=" + phone + "&debug=" + debug
	resBody, status, err := s.otpHandler("GET", url, nil)
	if err != nil {
		return nil, err
	}

	switch status {
	case 200:
		// unmarshal response data
		otpSrc := new(OTP)
		err = json.Unmarshal(resBody, &otpSrc)
		if err != nil {
			return nil, err
		}

		return otpSrc, nil
	default:
		return nil, errors.New(string(resBody))
	}
}

func (s *OTPClient) CheckOTP(otpDest *OTP) error {
	// marshal body
	reqBody, err := json.Marshal(otpDest)
	if err != nil {
		return err
	}

	// setup request
	url := s.endpoint + "/api/v1/otps"
	resBody, status, err := s.otpHandler("POST", url, reqBody)
	if err != nil {
		return err
	}

	switch status {
	case 200:
		return nil
	default:
		return errors.New(string(resBody))
	}
}

func (s *OTPClient) otpHandler(method string, url string, body []byte) ([]byte, int, error) {
	// setup request
	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, 0, err
	}

	// setup request header
	req.Header.Add("Content-Type", "application/json")

	// send request
	res, err := s.client.Do(req)
	if err != nil {
		return nil, 0, err
	}

	// read response body
	defer res.Body.Close()
	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, 0, err
	}

	return resBody, res.StatusCode, nil
}
